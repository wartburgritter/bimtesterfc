use_step_matcher("parse")
from bimtester.features.steps.aggregation import en

use_step_matcher("parse")
from bimtester.features.steps.application import de
from bimtester.features.steps.application import en

use_step_matcher("parse")
from bimtester.features.steps.attributes_eleclasses import de
from bimtester.features.steps.attributes_eleclasses import en

use_step_matcher("parse")
from bimtester.features.steps.attributes_psets import de
from bimtester.features.steps.attributes_psets import en

use_step_matcher("parse")
from bimtester.features.steps.attributes_qsets import de
from bimtester.features.steps.attributes_qsets import en

use_step_matcher("parse")
from bimtester.features.steps.classification import en

use_step_matcher("parse")
from bimtester.features.steps.element_classes import en

use_step_matcher("parse")
from bimtester.features.steps.geocoding import en

use_step_matcher("parse")
from bimtester.features.steps.geolocation import en

use_step_matcher("parse")
from bimtester.features.steps.geometric_detail import de
from bimtester.features.steps.geometric_detail import en

use_step_matcher("parse")
from bimtester.features.steps.geometric_freecad import de
from bimtester.features.steps.geometric_freecad import en

use_step_matcher("parse")
from bimtester.features.steps.layer_and_style import de
from bimtester.features.steps.layer_and_style import en

use_step_matcher("parse")
from bimtester.features.steps.materials import de
from bimtester.features.steps.materials import en

use_step_matcher("parse")
from bimtester.features.steps.model_federation import en

use_step_matcher("parse")
from bimtester.features.steps.project_setup import de
from bimtester.features.steps.project_setup import en
from bimtester.features.steps.project_setup import fr
from bimtester.features.steps.project_setup import it
from bimtester.features.steps.project_setup import nl

use_step_matcher("parse")
from bimtester.features.steps.spatial_structure import en
