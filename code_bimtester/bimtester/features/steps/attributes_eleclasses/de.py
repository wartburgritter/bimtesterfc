from behave import step


@step('Im Modell sind aussschliesslich "{ifc_entity_classes}" Objekte vorhanden')
def step_impl(context, ifc_entity_classes):
    context.execute_steps(f'* In the model are "{ifc_entity_classes}" objects only')


@step('Im Modell sind exakt "{count_exact}" "{ifc_entity_class}" Objekte vorhanden')
def step_impl(context, count_exact, ifc_entity_class):
    context.execute_steps(f'* In the model are precisely "{count_exact}" "{ifc_entity_class}" objects available')


@step('Im Modell sind zwischen "{count_min}" und "{count_max}" "{ifc_entity_class}" Objekte vorhanden')
def step_impl(context, count_min, count_max, ifc_entity_class):
    context.execute_steps(f'* In the model are between "{count_min}" and "{count_max}" "{ifc_entity_class}" objects available')


@step('Alle "{ifc_class}" Bauteile haben einen der folgenden Namen "{valuerange}"')
def step_impl(context, ifc_class, valuerange):
    context.execute_steps(f'* All "{ifc_class}" elements have one of these names "{valuerange}"')


@step('Es sind ausschliesslich "{ifc_classes}" Bauteile vorhanden')
def step_impl(context, ifc_classes):
    context.execute_steps(f'* There are exclusively "{ifc_classes}" elements only')


@step('Es sind keine "{ifc_class}" Bauteile vorhanden')
def step_impl(context, ifc_class):
    context.execute_steps(f'* There are no "{ifc_class}" elements')


@step('Aus folgendem Grund gibt es keine "{ifc_class}" Bauteile: {reason}')
def step_impl(context, ifc_class, reason):
    context.execute_steps(f'* There are no "{ifc_class}" elements because "{reason}"')


@step('Alle "{ifc_class}" Bauteilklassenattribute haben einen Wert')
def step_impl(context, ifc_class):
    context.execute_steps(f'* All "{ifc_class}" elements class attributes have a value')


@step('Alle "{ifc_class}" Bauteile haben einen Namen')
def step_impl(context, ifc_class):
    context.execute_steps(f'* All "{ifc_class}" elements have a name given')


@step('Bei allen "{ifc_class}" Bauteilen ist die Beschreibung angegeben')
def step_impl(context, ifc_class):
    context.execute_steps(f'* All "{ifc_class}" elements have a description given')


@step('Alle "{ifc_class}" Bauteile haben einen Namen mit dem Muster "{pattern}"')
def step_impl(context, ifc_class, pattern):
    context.execute_steps(f'* All "{ifc_class}" elements have a name matching the pattern "{pattern}"')
