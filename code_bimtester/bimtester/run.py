import os
import sys
import json
import shutil
import time
import logging
import tempfile
import ifcopenshell

try:
    import ifcopenshell.express
except:
    pass  # They are using an old version of IfcOpenShell. Gracefully degrade for now.
import behave.formatter.pretty  # Needed for pyinstaller to package it
from bimtester.ifc import IfcStore
from distutils.dir_util import copy_tree
from behave.__main__ import main as behave_main


# TODO: refactor when this isn't super experimental
from logging import StreamHandler

class IDSHandler(StreamHandler):
    def __init__(self):
        StreamHandler.__init__(self)
        self.results = {
            "name": "Specification name",
            "status": "passed",
            "location": "filename.xml",
            "elements": [
                {
                    "keyword": "Scenario",
                    "name": "Checking IDS specifications",
                    "status": "passed",
                    "steps": []
                }
            ]
        }

    def emit(self, record):
        msg = self.format(record)
        # Obviously, not a final product
        is_fail = "is compliant" not in msg
        if is_fail:
            self.results["status"] = "failed"
            self.results["elements"][0]["status"] = "failed"
        self.results["elements"][0]["steps"].append({
            "keyword": "*",
            "match": {},
            "name": msg,
            "result": {
                "duration": 0.0,
                "error_message": "Assertion Failed",
                "status": "failed" if is_fail else "passed"
            },
            "step_type": "given"
        })


class TestRunner:
    def __init__(self, ifc_path, schema_path=None, ifc=None, ifcstore=None):

        # can't load the IFC file if the schema is not loaded before
        if schema_path:
            schema = ifcopenshell.express.parse(schema_path)
            ifcopenshell.register_schema(schema)

        if ifcstore is not None:
            # use a instanz of IfcStore and add a init to class module
            IfcStore.path = ifcstore.path
            IfcStore.file = ifcstore.file
            IfcStore.bookmarks = ifcstore.bookmarks
            IfcStore.psets = ifcstore.psets
            IfcStore.qsets = ifcstore.qsets
        else:
            print("Parse IFC im BIMTester run module in der TestRunner Klasse")
            IfcStore.path = ifc_path
            time_start = time.process_time()
            IfcStore.file = ifc if ifc else ifcopenshell.open(ifc_path)
            # print(IfcStore.file.by_type("IfcProject")[0])
            from ifcopenshell.util.element import get_psets
            for elem in IfcStore.file.by_type("IfcBuildingElement"):
                IfcStore.psets[elem.id()] = get_psets(elem, psets_only=True)
                IfcStore.qsets[elem.id()] = get_psets(elem, qtos_only=True)
            print(
                "IFC parsing and getting psets time: {} seconds."
                .format(round((time.process_time() - time_start), 3))
            )

        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            self.base_path = sys._MEIPASS
        except Exception:
            self.base_path = os.path.dirname(os.path.realpath(__file__))

        self.locale_path = os.path.join(self.base_path, "locale")

    def run(self, args):
        if args["feature"][-4:].lower() == ".xml":
            return self.test_ids(args)
        return self.test_feature(args)

    def test_ids(self, args):
        # Local import whilst this is experimental
        import ifcopenshell.ids

        logger = logging.getLogger("IDS")
        logging.basicConfig(level=logging.INFO, format="%(message)s")
        ids_handler = IDSHandler()
        logger.addHandler(ids_handler)
        ids_file = ifcopenshell.ids.ids.open(args["feature"])
        ids_file.validate(IfcStore.file, logger)

        tmpdir = tempfile.mkdtemp()
        report_json = os.path.join(tmpdir, "report.json")
        json.dump([ids_handler.results], open(report_json, "w"))
        return report_json

    def test_feature(self, args):
        # tmpdir = tempfile.mkdtemp()
        tmpdir = os.path.join(tempfile.gettempdir(), "bimtesterfc")
        # delete tmpdir, if it exists
        if os.path.isdir(tmpdir):
            try:
                shutil.rmtree(tmpdir)
            except OSError as e:
                print("Error deleteing directory: %s - %s." % (e.filename, e.strerror))
                exit()
            os.mkdir(tmpdir)
        features_path = os.path.join(tmpdir, "features")
        steps_path = os.path.join(features_path, "steps")
        report_json = os.path.join(tmpdir, "report.json")
        shutil.copytree(os.path.join(self.base_path, "features"), features_path)
        if os.path.isfile(args["feature"]):
            shutil.copy(args["feature"], features_path)
        elif os.path.isdir(args["feature"]):
            copy_tree(args["feature"], features_path)
        if args["steps"]:
            if os.path.isfile(args["steps"]):
                shutil.copy(args["steps"], steps_path)
            elif os.path.isdir(args["steps"]):
                copy_tree(args["steps"], steps_path)
        args = self.get_behave_args(args, features_path, report_json)
        import json
        print("The behave args in run")
        print(json.dumps(args, indent=4))
        behave_main(args)
        return report_json  # is returned even if not created, (args["console"] == False)

    def get_behave_args(self, args, features_path, report_json):
        behave_args = [features_path]
        behave_args.extend(["--define", "localedir={}".format(self.locale_path)])
        if args["advanced_arguments"]:
            behave_args.extend(args["advanced_arguments"].split())
        if args["ifc"]:
            behave_args.extend(["--define", "ifc={}".format(args["ifc"])])
        if args["path"]:
            behave_args.extend(["--define", "path={}".format(args["path"])])
        if args["lang"]:
            behave_args.extend(["--lang={}".format(args["lang"])])
        if not args["console"]:
            # https://github.com/behave/behave/issues/346
            behave_args.extend(["--no-capture", "--format", "json.pretty", "--outfile", report_json])
        return behave_args
