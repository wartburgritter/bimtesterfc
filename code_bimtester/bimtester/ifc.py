class IfcStore:
    path = ""
    file = None
    bookmarks = {}
    psets = {}
    qsets = {}
    new_props = {}
    geom_freecad_shape = {}
    geom_has_elements = {}
    geom_has_errors = {}
