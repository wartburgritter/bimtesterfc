### test examples
+ very small ifc (just a box) and only the attributes needed for the test
+ very small feature file


### TODO
+ files for each test:
    + ifc
    + feature file
    + json report
+ script
    + run bimtester
    + compare resulting json report with saved json report
+ script for all test examples
+ real unit tests
